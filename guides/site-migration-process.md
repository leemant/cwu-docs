Regarding Sites that are being developed on tst and moved to production (tst -> prod)

Tools:
-  Use _Backup and Migrate_ (B&M) to backup the sites.
    - Settings: under 'advanced' use __bZip__ compression (gZip creates archives that are too big for the server to upload)
- If it's not already enabled, enable it
- Before you start backing up, you need to make sure that Backup and migreate enabled properly.
    - Check the restore option! If it's not there or throws an error, it needs to be re-installed:
        - diable module
        - uninstall module
        - flush caches
        - run cron
        - re-enable
        - flush cache
        - run cron

    This should take care of it. However, I've run into issues with needing to clear the cache/run cron multiple times to make sure there are no old variables and settings related to the previous instance of the module.

 ## Steps:
 ### 1. Backup
 1. create a folder for the site and then 2 subfolders. One for tst and one for prod.
    - ex. for site cwu.edu/xyz, the folder structure is:

            ```
            .
            └── xyz
                ├── prod
                │   ├── backup (for the B&M file)
                │   └── files (for the site files, cts.cwu.edu.xyz/files)
                └── tst
                    ├── backup
                    └── files
            ```

2. Backup the production database for site `cwu.edu/<site to migrate>` (Advanced -> use bZip)
3. Backup the tst database (Advanced -> use bZip)
4. Copy the files dir for /xyz on tst to the _files_ folder for __prod__
5. Copy the files dir for /xyz on tst to the _files_ folder for __tst__

### 2. Test the databases
1. Use a test container on prod such as /bienvenidos
2. Make sure Backup and Migrate is enabled
3. test the Prod database backup
4. test the tst database backup

If an issue arises, it's best to find it at this point. Check all the B&M settings, and modules that may be causing the error(s), then re-export.

Potential issues:
- the restore page is throwing an error/page error (see above: B&M tools: ...before you start backing up...)

### 3. Sync the tpls/theme/modules
1. Make sure the theme is current and any .tpl files used on the tst/dev version of the site are transferred over to the prod server
    - backing up the .tpls prod before overwriting is advised
2. Make sure the modules being used on the updated site are present in the modules for prod

Note that some modules should not be enabled for the 2019 theme:
- disble DHTML menu. It conflicts with how the 2019 menus should work. The workflow is different now because we use vanillia menus now. Top level links should be set to expanded for dropdowns.
    - check the menus to ensure all top level links with children are shown as expanded.
    - Note: Using 'show as expanded' is how the menu sys is supposed to function. (dhtml menu skipped all that (which is why it's got security issues) and created some bad habits for us)

### 4. Restore
1. Use Backup and Migrate to restore the tst database to prod.
2. move the files folder into the prod site dir (cts.cwu.edu/xyz)
    - try not to overwrite if it's not necessary. Best to only add what's not already in prod.

If there are issues, restore the backup from prod. I dentify the prob and try again.

### 5. QA
1. Pull up the tst version of the site for reference

On the prod version:

1. click 'content' on the admin menu.
    - filter by published
    - look through them all to make sure they're rendering properly.
1. Check that the Images are present for content header, UI, etc.
1. Check that styles are correct, paragraphs visible, etc.
1. Check though the menus, ensure the links are working.
1. Make sure the functionality is there. Js & CSS is working for menus, animations, placement. UI assets.
1. Update any issues that arise and review