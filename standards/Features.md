# Features 

### Featuring Paragraphs bundles

Paragraphs needs a few different things for a _complete_ feature module

Select the Paragraphs bundles you want to feature. 

Features will do its best to add in the dependencies, but falls short with paragraphs (especially since we have to take into account that not all sites will have the same active module as the container (i.e. /public-affairs) being featured from.

__Dependency checklist:__
