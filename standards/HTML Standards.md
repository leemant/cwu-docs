# HTML Standards

__HTML Class Standards__

Class standards include naming and css/scss structure & examples.

The goal is to provide a foundation for our devs to work from, making it easy for all of us here today, tomorrow, and 'outsiders' to read and understand the codebase.

Class naming is following a flavor of BEM suggested form seesparkbox.com

__Class Naming__

The basics, a tasty example:

Block-Element-Modifier
_a.k.a._ __BEM__

BEM provides us with a scaffolding for developing flat css files. Flat css is all celectors are of the same level of specificity. Here's a visual that should help to understand. The commonly accepted 'score' ![specificity](../assets/specifishity.jpg).

The reason we want this 'flat' specificity is so we can quickly identify, encapsulate, modify, and override easily. Basically, it keeps us young and our hair long.

With that said, css selectors should adhere to using one class only.


	// bad
	<div class="foo">
	  <a href="baz.html"> some link </a>
	</div>

	.foo {...} // specificity: 10
	.foo a {//targeting this anchor only} // specificity: 11

	// good
	<div class="foo">
	  <a class="foo__link" href="baz.html"> some link </a>
	</div>

	.foo {...} // specificity: 10
	.foo__link {//targeting this anchor only} // specificity: 10


A flavorful example of the basics of BEM
You have a container, but there are many types of containers (aka __Blocks__). However, you want this container for _icecream_. Therefore, we name the container `icecream`.

Our block will have a class of `icecream`

	<div class="icecream">
	...
	...
	</div>

Now, our icream container needs some icecream or else there's not much point to having just the container, amiright?

You can think of the flavor of icecream as the ____element__ within our BE(lement)M structure.

	<div class="icecream">
	  <div class="icecream__strawberry">Strawberry Icecream</div>
	  <div class="icecream__notstrawberry">notStrawberry Icecream</div>
	</div>



At this point you could stop, but in the case that you need a Modifier, the M in BEM, to alter an Element use the __--modifier__ syntax. A modifier is just that. In our example, we want to modify the strawberry icecream with some sprinkles. The base element is still the same, but it has a little something extra.

The full BEM syntax is block__element--modifier. Note: elements are preceded with two underscores __, and modifiers are preceded by two dashes --.


	<div class="icecream">
	  <div class="icecream__strawberry">Strawberry Icecream</div>
	  <div class="icecream__strawberry icecream__strawberry--sprinkles">Strawberry Icecream with sprinkles on top</div>
	</div>

This is all great, but there are some cases where there are more than 2 nested levels. Such as menus that are different enough to warrant classes for those additional nested elements. To resolve this, we continue to use BEM for nested elements greater than 2 levels deep.

	<ul class="menu">
	  <li class="menu__item menu__item--parent">
	    <ul class="menu menu--submenu">
	      <li class="menu__item menu__item--child">
	        <a class="menu__link">text</a>
	      </li>
	      <li class="menu__item menu__item--child">
	        <a class="menu__link">text</a>
	      </li>
	      <li class="menu__item menu__item--child">
	        <a class="menu__link menu__link--img">
	          <img class="menu__img" src="foo.png"/>
	        </a>
	      </li>
	    </ul>
	  </li>
	  <hr class="menu__rule" />
	</ul>