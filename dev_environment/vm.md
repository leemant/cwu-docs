# Virtual Machine and Operating System

### Create a Virtual Machine Using VirtualBox

Install [VirtualBox](https://www.virtualbox.org/) and download [ubuntu](https://www.ubuntu.com/download/desktop)

_Once you've installed virtual box:_

1. Click the __New__ button in the upper left to create a new VM
1. Name it Drupal Dev or something of that nature
1. select the type of operating system you're going to use
1. The recommended memory size is fine (2 GB)

    >__note:__ using too much memory will starve your host OS and slow the VM down.
    >It's recommended to use only what memory you need for best performance

1. Create a virtual hard disk
1. click create
1. Enter the folder path you want to use
1. Linux only uses a couple GBs of disk space for th OS. The default 10GB is fine.

    __Slower & flexible:__ choose dynamic

    __Performance:__ choose fixed

1. Fire up the VM after it's finished setup
1. It will prompt you to install an OS.

[Install Ubuntu]()
