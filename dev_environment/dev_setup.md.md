# Documentation


 This repository is intended for CWU Web Services living documentation for protocols, development environments, standards, etc.

 Softwares used shold be located either with available links, archived/transposed web pages, or be held in a shared drive or the like.

---

### __How to Create a Local Development Environment__

1. #### LAMP stack setup

    1. Setup a [virtural dev environment](dev_environment/dev_environment.md)
    1. Setup a [local web server](dev_environment/web_server.md)

1. #### Install Drupal, a Database for It, and

    1. installing [Drupal](drupal/drupal_installation.md)
    1. setting up a [database](drupal/database.md) for Drupal
