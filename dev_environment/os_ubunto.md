# Install Ubuntu OS

__download the [ubuntu](https://www.ubuntu.com/download/alternative-downloads) v18.04 iso file if you havent already__

>This tut is based on Ubuntu version __18.04__

1. (browse) choose the .iso
1. install ubuntu
1. continue with the keyboard layout
1. Normal installation and Download updates is good (continue)
1. leave _erase disk_ and __check__ _Use LVM_ with the new Ubuntu istallation
1. click _Install Now_
1. It'll show a dialog about the LVM partition changes and format, click continue
1. The installation process will begin
1. Choose your timezone and click continue
1. Fill in the fields
1. Your Name should be relevent
1. Click continue
1. the OS will finish installing
1. Restart the system
1. install updates

__Power off the machine.__
There are a couple other performance adjustments (well one big one)
In VirtualBox, right click your VM and choose settings
Go to the Advanced tab and select the processor tab
Try to give the VM the basic specs that Ubuntu requires

Under settings:
1. system tab
    >giving the OS at least 2+ cores is recommended
    >
    >don't exceed more then 50% of the physical cores
    >
    >if using an intel chip turn on hyper-v under acceleration

2. Display (your milage may vary)
    >Give it all the video memory (128 MB)
    >
    >\[x\] enable 3d acceleration

Your VM should be fairly responsive now. Furthermore, the VM isn't going to have the performance level of the host OS, so don't expect it to.

Next: Setup the local [web server](web_server.md)