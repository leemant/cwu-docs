# Install Drupal the `quicker` way


## Installation via terminal

1. Get the drupal archive

        wget https://www.drupal.org/files/projects/drupal-7.64.tar.gz

1. Extract the archive

        tar -zxvf drupal-7.64.tar.gz

1. Move the extracted drupal folder to the server root.

        mv drupal-7.64 /var/www/html

1. Rename the drupal-7.x folder to drupal

        mv /var/www/html/drupal-7.64 /var/www/html/drupal

1. Change dto the drupal site default directory

        cd /var/www/html/drupal/sites/default

1. Copy the settings file

        cp default.settings.php settings.php

1. Change the permissions to r/w for settings

        chmod a+w settings.php

1. Change default directory permissions to r/w

        chmod a+w /var/www/html/drupal/sites/default

cool. now:

The above files permission settings are for a secure setup. For development, that doesn't really matter. You can use:

    $ sudo chmod -R 777 /var/www/html

to allow r/w/e for all users.
