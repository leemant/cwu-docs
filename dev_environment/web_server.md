# Create a Web Server for ubuntu (LAMP) stack

This document uses a web resource that is no longer available. The guide  is credited to DigitalOcean

[Quick setup (recommended)](#recommended-for-quick-local-development)

[The long version that has additional security for internet use](#the-full)
_____

### __** Cliffnotes ver for a LOCALHOST install__
## Recommended for quick local development

1. Fire up Ubuntu on your VM
1. open up the terminal
1. __Install Apache__

        $ sudo apt update
        $ sudo apt install apache2

        Apache installing ...
        ...
        Press Y then Enter

    Check to see that you get the apache page

        http://your_ip_address
        a.k.a. localhost

2. __Install MySQL__

        $ sudo apt update
        $ sudo apt install mysql-server
        $ sudo mysql_secure_installation

Validate Password Plugin -> Y
Choose a level of PW security (this will throw an error if your pw sux) (if local only - 0 is fine)
Give ROOT a pw (don't forget it, so maybe use the login pw)
Enter Y for the rest of the security prompts
<<< See full instructions above for securing mysql

3. ### Install PHP

        $ sudo apt install php libapache2-mod-php php-mysql php-gd php-xml

Make apache look at index.php files first in the waterfall

        $ sudo nano /etc/apache2/mods-enabled/dir.conf

Old:

        <IfModule mod_dir.c>
        DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm
        </IfModule>

New: (put .php first)

        <IfModule mod_dir.c>
        DirectoryIndex index.php index.html index.cgi index.pl  index.xhtml index.htm
        </IfModule>
        $sudo systemctl restart apache2

Test install

        $sudo nano /var/www/html/info.php

Add to the file:

        <?php
        phpinfo();
        ?>

Open a browser and nav to:

[localhost/info.php](http://localhost/info.php)

You should see the php info screen

If not, there is a problem with the install
 - were there any errors when installing apache, mysql, and php?

Finally, remove the info.php file

        $ sudo rm /var/www/html/info.php

That takes care of setting up a LAMP stack, yay

Now, a little quality of life option:

    $ sudo chmod -R 777 /var/www/html

_____

## The full

### How To Install Linux, Apache, MySQL, PHP (LAMP) stack on Ubuntu 18.04

Posted April 27, 2018 51.3k views LAMP Stack PHP MySQL Apache Ubuntu 18.04



From [Digital Ocean LAMP stack tut](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-ubuntu-18-04)
