# Install Drupal (v7.x)

[Quick development setup](#quick-development-setup)

[Drupal installation](#drupal-installation)

## Quick development setup
If you're only using a localhost install, you can get away with setting the var/www/html directory to allow write access.

    To enable write acces to be able to easily drag, drop, and create files:

        $ sudo chmod -R 777 var/www/html

## Drupal installation

[Install it in a few commands via cli](d7_terminal)

or

1. Download and instal [Drupal 7](https://www.drupal.org/project/drupal/releases?api_version%5B%5D=103)
1. Open it with the archive manager
1. Right click on the folder and click extract
1. extract to /var/www/html/
1. Rename the folder __drupal__

    >Run the command below again.
    >
    >       $ sudo chmod -R 777 var/www/html

1. Copy the ./sites/default/default.settings.php to ./sites/default/settings.php

---

1. [Create a database for drupal](database.md)
1. Begin the install process by navigating to [localhost/drupal/index.php](http://localhost/drupal/index.php) in your browser
1. Select _Standard_ installation

    Regarding installation -

        Depending on the version of php, the syntax and packages modules may change. Therefore, you may need to add the modules in manually.

        When you begin the installation process for drupal, it will let you know if you're missing any dependcies

        if you have trouble with write access, chmod /html again with the command above

1. accept language option
1. __DO NOT PROCEED WITH THE INSTALLATION WITH ANY ERRORS!!!__

    Scrap the drupal installation if you happen to continue with errors (warings are arguably ok)

1. Add in the DB name and your mySQL user and pw, continue
1. Give the install a site a name, email.. fill it out :)
1. Visit your new site (it should work)
1. This part of the Drupal installation is complete