# Creating a database for Drupal 7

[CLI guide](#cli-instructions)

---

## CLI Instructions

1.  create the database using the following commands

    login to mysql: (you may need to prefix this with sudo)

        $ sudo mysql -u root -p
            it will ask for both your user pw and mysql user pw

        mysql> CREATE DATABASE drupal_dev CHARACTER SET utf8 COLLATE utf8_general_ci;

        if not within mysql already then you can use:

        mysql -u root -p -e "CREATE DATABASE drupal_dev CHARACTER SET utf8 COLLATE utf8_general_ci";


        mysql -u root -p

        CREATE USER drupal@localhost IDENTIFIED BY 'drupal';
        //if you get some crap about the password policy type the command: uninstall plugin validate_password

        GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON databasename.* TO 'username'@'localhost' IDENTIFIED BY 'password';

On success you'll see

        Query OK, 0 rows affected

Type in to quit the sql cmd interface

        exit