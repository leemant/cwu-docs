# Setting up a development Virtual Machine

cwu_2019 theme uses [Foundation 6](https://foundation.zurb.com/sites/docs/) for its framework

We use Red Hat for server, so ubuntu will serve fine for a basic dev environment.

At the time of this writing:
- [Ubuntu](https://www.ubuntu.com/download/alternative-downloads) ver 18.04 is used
- [VirtualBox](https://www.virtualbox.org/wiki/Download_Old_Builds_5_2) is ver 5.2
- [Drupal 7.59](https://www.drupal.org/project/drupal/releases/7.59)